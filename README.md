# RouterApp

#### 介绍
三种经典路由介绍，URL路由，Protocol路由 ， Target-Action路由

# Target-Action
## 优点
    1. 无需注册路由，避免了load里注册影响启动速度
    2. 不用维护全局路由表，避免了查找对应路由
    3. 统一了路由入口
    4. 进行了一定的安全处理
## 缺点
    1. 不能多端使用同一套路由H5，Android
    2. 增加了文件，同时在获取target，sel时存在一定的硬编码
## 注意事项
	通过NSInvocation的getReturnValue时，如果是对象类型，在用id类型接受时，引用计数没有加一，需要特殊处理；直接使用可能造成Crash。可以通过 (__bridge id)xxx 来解决内存管理的问题
	 ```
	     if (strcmp(signature.methodReturnType, @encode(NSString)) == 0) {
        NSInvocation *invocation = [[NSInvocation alloc] init];
        [invocation setArgument:&params atIndex:2];
        invocation.target = target;
        id result;
        [invocation invoke];
        [invocation getReturnValue:&result];        
        return result;
    }
	 ```
# Protocol路由
## 优点
	1. 符合swift的思想， 面向协议编程
	2. 安全性高，在编译阶段就可以检测出问题
	3. 模块解耦
## 缺点
	1. 无法多端通用
	2. 需要进行协议的注册

# URL路由
## 优点
    1. 多端通用
    2. 统一路由入口
    3. 学习成本低，易使用
## 缺点
    1. 需要load注册
    2. 涉及硬编码
    3. 只能通过字典传参

