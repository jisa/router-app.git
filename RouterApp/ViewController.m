//
//  ViewController.m
//  RouterApp
//
//  Created by jisa on 2022/6/6.
//

#import "ViewController.h"
#import "URLRouter.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[URLRouter shared] openRouterWithURL:@"native://chaorenhui/home/Thire" userInfo:@{@"name":@"李德省"} complete:^(id  _Nonnull obj) {
            UIViewController *vc = obj;
            vc.title = @"李德胜";
            [self.navigationController pushViewController:vc animated:true];
            NSLog(@"打开完成");
        }];
    });
    // Do any additional setup after loading the view.
}


@end
