//
//  FFTargetActionRouterMediator+Module.m
//  RouterApp
//
//  Created by jisa on 2022/6/7.
//
#import <UIKit/UIKit.h>
#import "FFTargetActionRouterMediator+Module.h"

@implementation FFTargetActionRouterMediator (Module)
- (UIViewController *)acquireFavoriteSecondVCWithPara:(NSDictionary *)dic {
    return [self performTargetName:@"FavoriteModuleHandle" actionName:@"AcquireSecondViewControllerWith:" params:dic];
}

- (NSInteger)acquireSum {
    NSInteger y = [[self performTargetName:@"FavoriteModuleHandle" actionName:@"SUM:" params:@{@"sum":@0}] integerValue];
    return y;
}
@end
