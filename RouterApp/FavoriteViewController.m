//
//  FavoriteViewController.m
//  RouterApp
//
//  Created by jisa on 2022/6/6.
//

#import "FavoriteViewController.h"
#import "FFTargetActionRouterMediator.h"
#import "FFTargetActionRouterMediator+Module.h"
@interface FavoriteViewController ()

@end

@implementation FavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

- (void)tap {
    
    NSLog(@"2 + 0 = %ld", [[FFTargetActionRouterMediator shared] acquireSum]);
    
    UIViewController *vc = [[FFTargetActionRouterMediator shared] acquireFavoriteSecondVCWithPara:@{@"title":@"Target-Action"}];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
