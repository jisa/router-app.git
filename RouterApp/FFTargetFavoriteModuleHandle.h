//
//  FFTargetFavoriteModuleHandle.h
//  RouterApp
//
//  Created by jisa on 2022/6/7.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface FFTargetFavoriteModuleHandle : NSObject
- (UIViewController *)FFActionAcquireSecondViewControllerWith:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
