//
//  URLRouter.m
//  RouterApp
//
//  Created by jisa on 2022/6/9.
//

#import "URLRouter.h"

typedef id(^URLRouterReturnValueBlock)(NSMutableDictionary *mdic);
typedef void(^URLRouterEmpleyBlock)(NSMutableDictionary *mdic);

const NSString *URLRouterMinimumLevelKey = @"-";
NSString * const SpecialCharacter = @"/?&.";
NSString * const URLRouterUSERINFO = @"URLRouterUSERINFO";
NSString * const URLRouterComplete = @"URLRouterComplete";

@interface URLRouter ()
@property (nonatomic, strong) NSMutableDictionary *blockDic;
@end

@implementation URLRouter

+ (instancetype)shared {
    static URLRouter *mananger = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mananger = [super allocWithZone:nil];
        mananger.blockDic = [@{} mutableCopy];
    });
    return mananger;
}

//- (void)registerObjWithURL:(NSString *)url withoutResultBlock:(void(^)(NSMutableDictionary *mdic))block
//{
//    
//    if ([self judgeNoEmptyStringWith:url] == NO) {
//        return;
//    }
//    NSMutableDictionary *mdic = [self acqurieReleateRouteTableWith:url];
//    if (block) {
//        mdic[URLRouterMinimumLevelKey] = [block copy];
//    }
//}

- (void)registerObjWithURL:(NSString *)url resultBlock:(id(^)(NSMutableDictionary *mdic))block
{
    if ([self judgeNoEmptyStringWith:url] == NO) {
        return;
    }
    NSMutableDictionary *mdic = [self acqurieReleateRouteTableWith:url];
    if (block) {
        mdic[URLRouterMinimumLevelKey] = [block copy];
    }
}

- (id)acqurieObjWithURL:(NSString *)url andUserInfo:(NSDictionary *)userInfo {
    NSMutableDictionary *mdic = [self acquireCompleteURLInfoWithRouter:url isExact:YES];
    if (userInfo.count > 0) {
        mdic[URLRouterUSERINFO] = userInfo;
    }
    if (mdic.count == 0) {
        return nil;
    }
    if (mdic[URLRouterMinimumLevelKey]) {
        URLRouterReturnValueBlock block = mdic[URLRouterMinimumLevelKey];
        block(mdic);
    }
    return nil;
}

- (BOOL)judgeCanOpenPageWithURL:(NSString *)url {
    return [self acquireCompleteURLInfoWithRouter:url isExact:YES].count > 0 ? YES : NO;
}

/// 打开路由
- (id)openRouterWithURL:(NSString *)url userInfo:(NSDictionary *)userInfo complete:(void(^)(id obj))complete {
    NSMutableDictionary *paraDic = [self acquireCompleteURLInfoWithRouter:url isExact:YES];
    if (paraDic.count > 0) {
        paraDic[URLRouterUSERINFO] = userInfo;
        paraDic[URLRouterComplete] = [complete copy];
    }
    if (paraDic[URLRouterMinimumLevelKey]) {
        URLRouterReturnValueBlock block = paraDic[URLRouterMinimumLevelKey];
        return block(paraDic);
    }
    return nil;
}

/// 删除注册的路由
- (void)removeRegisterRouterWithURL:(NSString *)url {
    if ([self judgeNoEmptyStringWith:url] == NO) {
        return;
    }
    if ([self acquireCompleteURLInfoWithRouter:url isExact:YES].count == 0) {
        return;
    }
    NSArray *pathArr = [self acquirePathArrWith:url];
    NSMutableDictionary *mdic = [self.blockDic valueForKeyPath:[pathArr componentsJoinedByString:@"."]];
    if (mdic.count > 0) {
        NSMutableArray *marr = [pathArr mutableCopy];
        NSString *last = [marr lastObject];
         [marr removeLastObject];
        if (marr.count > 0) {
            mdic = [self.blockDic objectForKey:[marr componentsJoinedByString:@"."]];
        }
        [mdic removeObjectForKey:last];
    }
}

/// 获取完整的URL信息
- (NSMutableDictionary *)acquireCompleteURLInfoWithRouter:(NSString *)router isExact:(BOOL)isExact {
    NSMutableDictionary *mdic = [@{} mutableCopy];
    if ([self judgeNoEmptyStringWith:router] == NO) {
        return mdic;
    }
    
    mdic[@"ROUTER"] = router;
    
    NSArray *pathArr = [self acquirePathArrWith:router];
    NSMutableDictionary *routerDic = self.blockDic;
    for (NSString *segment in pathArr) {
        NSArray *keyArr = [routerDic.allKeys sortedArrayUsingComparator:^NSComparisonResult(NSString   *obj1, NSString *obj2) {
            return [obj1 compare:obj2];
        }];
        BOOL isFound = NO;
        for (NSString *key in keyArr) {
            if ([key isEqualToString:segment]) {
                routerDic = routerDic[key];
                isFound = YES;
                break;
            } else if ([key hasPrefix:@":"]) {
                routerDic = routerDic[key];
                NSString *newKey = [key substringFromIndex:1];
                NSString *newPath = segment;
                if ([self judgeSpecialCharacterWith:newPath]) {
                    NSRange range = [newPath rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:SpecialCharacter]];
                    newKey = [newPath substringToIndex:range.location - 1];
                    newPath = [newPath stringByReplacingCharactersInRange:range withString:@""];
                }
                isFound = YES;
                mdic[newKey] = newPath;
                break;
            } else if (isExact){
                isFound = NO;
                continue;
            }
        }
        
        if (isExact && isFound == NO) {
            return [@{} mutableCopy];
        }
    }
    mdic[URLRouterMinimumLevelKey] = [routerDic[URLRouterMinimumLevelKey] copy];
    NSURL *url = [self convertURLFromString:router];
    NSArray<NSURLQueryItem *> *items = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:NO].queryItems;
    for (NSURLQueryItem *item in items) {
        mdic[item.name] = [item.value ? : @"" stringByRemovingPercentEncoding];
    }
    
    return mdic;
}

/// 校验特殊字符
- (BOOL)judgeSpecialCharacterWith:(NSString *)str {
    return [str rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:SpecialCharacter]].location != NSNotFound;
}

/// 讲字符串转化成URL 同时进行URLQuery编码
- (NSURL *)convertURLFromString:(NSString *)str {
    return [NSURL URLWithString:[str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
}

/// 获取对应的路由映射表。
- (NSMutableDictionary *)acqurieReleateRouteTableWith:(NSString *)router {
    NSArray *pathArr = [self acquirePathArrWith:router];
    NSMutableDictionary *mdic = self.blockDic;
    for (NSString *path in pathArr) {
        if ([mdic.allKeys containsObject:path] == NO) {
            mdic[path] = [@{} mutableCopy];
        }
        mdic = mdic[path];
    }
    return mdic;
}

/// 判断是否是非空字符串
- (BOOL)judgeNoEmptyStringWith:(NSString *)str {
    if ([str isEqual:[NSNull null]]) {
        return NO;
    }

    if (![str isKindOfClass:NSString.class]) {
        return NO;
    }
    
    if ([str isEqualToString:@"null"]) {
        return NO;
    }
    
    return str.length > 0;
}

- (NSArray *)acquirePathArrWith:(NSString *)url {
    if (![self judgeNoEmptyStringWith:url]) {
        return @[@"~"];
    }
    
    NSURL *needURL = [NSURL URLWithString:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    NSMutableArray *pathArr = [@[] mutableCopy];
    [pathArr addObject:needURL.host];
    NSMutableArray *segmentArr = [[needURL.path componentsSeparatedByString:@"/"] mutableCopy];
    [segmentArr filterUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != %@", @""]];
    [pathArr addObjectsFromArray:segmentArr];
    if (pathArr.count == 0) {
        [pathArr addObject:@"~"];
    }
    return [NSArray arrayWithArray:pathArr];
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    return [self shared];
}

- (id)copy {
    return self;
}

- (id)mutableCopy {
    return  self;
}

@end
