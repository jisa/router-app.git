//
//  URLRouter.h
//  RouterApp
//
//  Created by jisa on 2022/6/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString *const URLRouterUSERINFO;
extern NSString *const URLRouterComplete;

@interface URLRouter : NSObject

+ (instancetype)shared;

///// URL的路由地址，URL不接受参数
//- (void)registerObjWithURL:(NSString *)url withoutResultBlock:(void(^)(NSMutableDictionary *mdic))block;

/// URL的路由地址，URL不接受参数
- (void)registerObjWithURL:(NSString *)url resultBlock:(id(^)(NSMutableDictionary *mdic))block;

/// 获取要执行的对象
- (id)acqurieObjWithURL:(NSString *)url andUserInfo:(NSDictionary *)userInfo;

/// 是否能打开页面
- (BOOL)judgeCanOpenPageWithURL:(NSString *)url;

/// 打开路由
- (id)openRouterWithURL:(NSString *)url userInfo:(NSDictionary *)userInfo complete:(void(^)(id obj))complete;


/// 删除注册的路由
- (void)removeRegisterRouterWithURL:(NSString *)url;
@end

NS_ASSUME_NONNULL_END
