//
//  ProtocolRouterMediator.m
//  RouterApp
//
//  Created by jisa on 2022/6/8.
//

#import "ProtocolRouterMediator.h"

@interface ProtocolRouterMediator ()
@property (nonatomic, strong) NSMutableDictionary *targetDic;
@end

@implementation ProtocolRouterMediator
+ (instancetype)shared {
    static ProtocolRouterMediator *mediator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mediator = [super allocWithZone:nil];
        mediator.targetDic = [@{} mutableCopy];
    });
    return mediator;
}

- (void)registerImplementor:(id<BasicRouterProtocol>)implementor protocol:(Protocol *)protocol
{
    NSString *key = NSStringFromProtocol(protocol);
    if (implementor != nil) {
        self.targetDic[key] = implementor;
    }
}

///
- (id)acquireImplementorWithProtocol:(Protocol *)protocol
{
    NSString *key = NSStringFromProtocol(protocol);
    return self.targetDic[key];
}



+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    return [self shared];
}

- (id)copy {
    return self;
}

- (id)mutableCopy {
    return self;
}

@end
