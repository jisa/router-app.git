//
//  ProtocolRouterMediator.h
//  RouterApp
//
//  Created by jisa on 2022/6/8.
//

#import <UIKit/UIKit.h>
#import "BasicRouterProtocol.h"
NS_ASSUME_NONNULL_BEGIN


@interface ProtocolRouterMediator : NSObject

///
+ (instancetype)shared;

///
- (void)registerImplementor:(id<BasicRouterProtocol>)implementor protocol:(Protocol *)protocol;

///
- (id)acquireImplementorWithProtocol:(Protocol *)protocol;
@end

NS_ASSUME_NONNULL_END
