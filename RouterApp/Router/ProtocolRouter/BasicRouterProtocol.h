//
//  BasicRouterProtocol.h
//  RouterApp
//
//  Created by jisa on 2022/6/8.
//

#import <UIKit/UIKit.h>

@protocol BasicRouterProtocol <NSObject>

@required
- (UIViewController *)acquireViewControllerWithParas:(NSDictionary *)paras;

@end
