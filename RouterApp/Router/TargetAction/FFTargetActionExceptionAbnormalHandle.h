//
//  FFTargetActionExceptionAbnormalHandle.h
//  RouterApp
//
//  Created by jisa on 2022/6/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FFTargetActionExceptionAbnormalHandle : NSObject

/// 异常方法处理
/// @param paras @{@"target":targetName, @"action":actionName, @"para":paras}
- (void)abnormalMethodExecuteOf:(NSDictionary *)paras;
@end

NS_ASSUME_NONNULL_END
