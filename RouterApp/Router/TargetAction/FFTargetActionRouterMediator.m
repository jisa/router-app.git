//
//  FFTargetActionRouterMediator.m
//  RouterApp
//
//  Created by jisa on 2022/6/6.
//

#import "FFTargetActionRouterMediator.h"
#import <objc/runtime.h>
#import <UIKit/UIKit.h>
@interface FFTargetActionRouterMediator ()
@property (nonatomic, strong) NSMutableDictionary *cacheTargetDic;
@end

@implementation FFTargetActionRouterMediator
+ (instancetype)shared
{
    static FFTargetActionRouterMediator *mediator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mediator = [super allocWithZone:nil];
        mediator.defaultCacheTarget = YES;
        mediator.cacheTargetDic = [@{} mutableCopy];
    });
    return mediator;
}

#pragma mark -- 核心
/// 通过URL方式调用
- (id)performWithURL:(NSURL *)url completion:(void(^)(NSDictionary *))completion
{
    // 获取参数
    NSArray *queryArr = [url.query componentsSeparatedByString:@"&"];
    NSMutableDictionary *parasDic = [@{} mutableCopy];
    for (NSString *itemStr in queryArr) {
        NSArray *itemArr = [itemStr componentsSeparatedByString:@"="];
        if (itemArr.count == 2) {
            [parasDic setObject:itemArr[1] forKey:itemArr[0]];
        }
    }
    
    // 简单的安全处理，防止外部直接调用app
    NSString *actionName = [url.path stringByReplacingOccurrencesOfString:@"/" withString:@""];
    if ([actionName hasPrefix:@"native"]) {
        return @(NO);
    }
            
    id result = [self performTargetName:url.host actionName:actionName params:parasDic];
    if (completion) {
        if (result) {
            completion(@{@"result": result});
        } else {
            completion(nil);
        }
    }
    return result;
}

/// 内部使用 利用runtime 获取target，action
- (id)performTargetName:(NSString *)targetName actionName:(NSString *)actionName params:(NSDictionary *)params
{
    NSString *targetNameStr = [NSString stringWithFormat:@"FFTarget%@", targetName];
    NSObject *target = self.cacheTargetDic[targetNameStr];
    if (target == nil) {
        target = [[NSClassFromString(targetNameStr) alloc] init];
    }
    if (self.defaultCacheTarget) {
        [self.cacheTargetDic setObject:target forKey:targetNameStr];
    }
    
    NSString *actionNameStr = [NSString stringWithFormat:@"FFAction%@", actionName];
    SEL action = NSSelectorFromString(actionNameStr);
    
    if (target == nil) {
        [self withoutResponseTargetOrActionWithTarget:targetName action:actionName params:params];
        return nil;
    } else {
        if ([target respondsToSelector:action]) {
            return [self safePerformTarget:target action:action params:params];
        } else if ([target respondsToSelector:NSSelectorFromString(@"notFound:")]) {
            return [self safePerformTarget:target action:NSSelectorFromString(@"notFound:") params:params];
        } else {
            [self withoutResponseTargetOrActionWithTarget:targetName action:actionName params:params];
            return nil;
        }
    }
    
    return nil;
}

/// 异常处理，存在不能执行的action
- (id)withoutResponseTargetOrActionWithTarget:(NSString *)targetName action:(NSString *)actionName params:(NSDictionary *)params {
    
    NSObject *obj = [[NSClassFromString(@"FFTargetActionExceptionAbnormalHandle") alloc] init];

    SEL action = NSSelectorFromString(@"abnormalMethodExecuteOf:");
    
    NSDictionary *dic = @{
        @"target":targetName,
        @"action":actionName,
        @"para":params
    };
    return [self safePerformTarget:obj action:action params:dic];
}

- (id)safePerformTarget:(NSObject *)target action:(SEL)action params:(NSDictionary *)params {
    NSMethodSignature *signature = [target methodSignatureForSelector:action];
    if (signature == nil) {
        return nil;
    }
    const char * type = signature.methodReturnType;
    if (strcmp(type, @encode(void)) == 0) {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
        invocation.target = target;
        invocation.selector = action;
        [invocation setArgument:&params atIndex:2];
        [invocation invoke];
        return nil;
    }
    
    if (strcmp(type, @encode(BOOL)) == 0)
    {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
        invocation.target = target;
        invocation.selector = action;
        [invocation setArgument:&params atIndex:2];
        [invocation invoke];
        BOOL result = NO;;
        if ([signature methodReturnLength] > 0) {
            [invocation getReturnValue:&result];
            return @(result);
        }
    }
    
    if (strcmp(type, @encode(NSInteger)) == 0)
    {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
        [invocation setArgument:&params atIndex:2];
        invocation.selector = action;
        [invocation invokeWithTarget:target];
        NSInteger result = 0;
        if ([signature methodReturnLength] > 0) {
            [invocation getReturnValue:&result];
            return @(result);
        }
    }
    
    if (strcmp(type, @encode(int)) == 0)
    {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
        invocation.target = target;
        invocation.selector = action;
        [invocation setArgument:&params atIndex:2];
        [invocation invoke];
        int result = 0;;
        if ([signature methodReturnLength] > 0) {
            [invocation getReturnValue:&result];
            return @(result);
        }
    }
    
    if (strcmp(type, @encode(CGFloat)) == 0)
    {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
        invocation.target = target;
        invocation.selector = action;
        [invocation setArgument:&params atIndex:2];
        [invocation invoke];
        CGFloat result = 0.0;;
        if ([signature methodReturnLength] > 0) {
            [invocation getReturnValue:&result];
            return @(result);
        }
    }
    
    if (strcmp(type, @encode(double)) == 0)
    {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
        invocation.target = target;
        invocation.selector = action;
        [invocation setArgument:&params atIndex:2];
        [invocation invoke];
        BOOL result = NO;;
        if ([signature methodReturnLength] > 0) {
            [invocation getReturnValue:&result];
            return @(result);
        }
    }

    // 只能接受一个参数，如果想处理多个参数，使用 NSInvocation的invoke形式
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    return [target performSelector:action withObject:params];
#pragma clang diagnostic pop
}

/// 移除换成的target
- (void)removeCahceTargetWithTargetName:(NSString *)targetName
{
    
    [self.cacheTargetDic removeObjectForKey:[NSString stringWithFormat:@"FFTarget%@", targetName]];
}


#pragma mark -- 冗余处理
+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    return [self shared];
}

- (id)copy {
    return self;
}

- (id)mutableCopy {
    return self;
}
@end
