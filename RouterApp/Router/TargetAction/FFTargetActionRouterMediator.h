//
//  FFTargetActionRouterMediator.h
//  RouterApp
//
//  Created by jisa on 2022/6/6.
//

/*
 Target-Action 路由
 优点：
 1: 不需要路由注册，不用维护路由映射表，避免了查表
 2: 同一组件的维护入口
 3: 可以进行安全校验，进行容错处理
 缺点：
 1: 不能多端使用 H5， Android
 2: 增加代码量，需要维护对应的方法， 一个统一扩展入口， 一个对应模块的统一处理入口
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FFTargetActionRouterMediator : NSObject

/// 是否缓存target  默认yes
@property (nonatomic, assign) BOOL defaultCacheTarget;

+ (instancetype)shared;

/// 通过URL方式调用
- (id)performWithURL:(nonnull NSURL *)url completion:(void(^)(NSDictionary *))completion;

/// 内部使用 利用runtime 获取target，action
- (id)performTargetName:(nonnull NSString *)targetName actionName:(nonnull NSString *)actionName params:(nullable NSDictionary *)params;

/// 移除换成的target
- (void)removeCahceTargetWithTargetName:(nonnull NSString *)targetName;

@end

NS_ASSUME_NONNULL_END
