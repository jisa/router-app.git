//
//  FFTargetActionExceptionAbnormalHandle.m
//  RouterApp
//
//  Created by jisa on 2022/6/6.
//

#import "FFTargetActionExceptionAbnormalHandle.h"

@implementation FFTargetActionExceptionAbnormalHandle
- (void)abnormalMethodExecuteOf:(NSDictionary *)paras {
#if DEBUG
    NSLog(@"target = %@  action = %@  para = %@", paras[@"target"], paras[@"action"], paras[@"para"]);
    NSAssert(NO, @"没有对应的target或者action大佬🧍‍♂️");
#endif
}
@end
