//
//  FFTargetFavoriteModuleHandle.m
//  RouterApp
//
//  Created by jisa on 2022/6/7.
//

#import "FFTargetFavoriteModuleHandle.h"
#import <objc/runtime.h>
@implementation FFTargetFavoriteModuleHandle
- (UIViewController *)FFActionAcquireSecondViewControllerWith:(NSDictionary *)dic {
    UIViewController *vc = [[NSClassFromString(@"FavoriteSecondViewController") alloc] init];
    [vc setTitle:dic[@"title"]];
    return vc;
}

- (NSInteger)FFActionSUM:(NSDictionary *)dic {
    NSInteger sum = [dic[@"sum"] integerValue];
    NSInteger temp = sum + 2;
    return temp;
}
@end
