//
//  MineThirdViewController.h
//  RouterApp
//
//  Created by jisa on 2022/6/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineThirdViewController : UIViewController
@property (nonatomic, strong) NSString *name;
@end

NS_ASSUME_NONNULL_END
