//
//  FFTargetActionRouterMediator+Module.h
//  RouterApp
//
//  Created by jisa on 2022/6/7.
//

#import "FFTargetActionRouterMediator.h"

NS_ASSUME_NONNULL_BEGIN

@interface FFTargetActionRouterMediator (Module)
- (UIViewController *)acquireFavoriteSecondVCWithPara:(NSDictionary *)dic;

- (NSInteger)acquireSum;
@end

NS_ASSUME_NONNULL_END
