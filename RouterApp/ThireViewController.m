//
//  ThireViewController.m
//  RouterApp
//
//  Created by jisa on 2022/6/14.
//

#import "ThireViewController.h"
#import "URLRouter.h"

@interface ThireViewController ()

@end

@implementation ThireViewController
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[URLRouter shared] registerObjWithURL:@"native://chaorenhui/home/Thire" resultBlock:^id _Nonnull(NSMutableDictionary * _Nonnull mdic) {
            void(^complte)(id obj) = mdic[URLRouterComplete];
            id obj = [self new];
            complte(obj);
            return obj;
        }];
    });
}


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:arc4random() % 256 / 256.0 green:arc4random() % 256 / 256.0 blue:arc4random() % 256 / 256.0 alpha:1.0];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[URLRouter shared] openRouterWithURL:@"native://home/Second/:20" userInfo:@{} complete:^(id  _Nonnull obj) {
            UIViewController *vc = obj;
            vc.title = @"刘少奇";
            [self.navigationController pushViewController:vc animated:YES];
        }];
    });
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
