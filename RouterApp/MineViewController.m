//
//  MineViewController.m
//  RouterApp
//
//  Created by jisa on 2022/6/6.
//

#import "MineViewController.h"
#import "FFProtocolMineHandle.h"
#import "ProtocolRouterMediator.h"
@interface MineViewController ()

@end

@implementation MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)settingPage:(id)sender {
    id<FFMineProtocol> obj = [[ProtocolRouterMediator shared] acquireImplementorWithProtocol:NSProtocolFromString(@"FFMineProtocol")];
    UIViewController *vc = [obj acquireMineSettingPageWithParas:@{@"name":@"设置中心",@"bgColor":[UIColor magentaColor]}];
    [self.navigationController pushViewController:vc animated:true];
    
}

- (IBAction)qrCodePage:(id)sender {
    id<FFMineProtocol> obj = [[ProtocolRouterMediator shared] acquireImplementorWithProtocol:NSProtocolFromString(@"FFMineProtocol")];
    UIViewController *vc = [obj acquireQRCodePageWithParas:@{@"name":@"二维码扫描",@"bgColor":[UIColor systemGray6Color]}];
    [self.navigationController pushViewController:vc animated:true];
}

@end
