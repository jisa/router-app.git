//
//  SecondViewController.m
//  RouterApp
//
//  Created by jisa on 2022/6/14.
//

#import "SecondViewController.h"
#import "URLRouter.h"
@interface SecondViewController ()

@end

@implementation SecondViewController

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[URLRouter shared] registerObjWithURL:@"native://home/Second/:level" resultBlock:^id _Nonnull(NSMutableDictionary * _Nonnull mdic) {
            id ojb = [self new];
            void(^complete)(id obj) = mdic[URLRouterComplete];
            complete(ojb);
            return ojb;
        }];
    });
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:arc4random() % 256 / 256.0 green:arc4random() % 256 / 256.0 blue:arc4random() % 256 / 256.0 alpha:1.0];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
