//
//  FFProtocolMineHandle.h
//  RouterApp
//
//  Created by jisa on 2022/6/8.
//

#import <UIKit/UIKit.h>
#import "BasicRouterProtocol.h"
NS_ASSUME_NONNULL_BEGIN


@protocol FFMineProtocol <NSObject, BasicRouterProtocol>
/// 设置页面
- (UIViewController *)acquireMineSettingPageWithParas:(NSDictionary *)paras;
/// 二维码页面
- (UIViewController *)acquireQRCodePageWithParas:(NSDictionary *)paras;
@end

@interface FFProtocolMineHandle : NSObject<FFMineProtocol>

@end

NS_ASSUME_NONNULL_END
