//
//  FFProtocolMineHandle.m
//  RouterApp
//
//  Created by jisa on 2022/6/8.
//

#import "FFProtocolMineHandle.h"
#import "ProtocolRouterMediator.h"
#import "MineViewController.h"
#import "MineSecondViewController.h"
@implementation FFProtocolMineHandle
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[ProtocolRouterMediator shared] registerImplementor:[[self alloc] init] protocol:NSProtocolFromString(@"FFMineProtocol")];
    });
}

/// 获取我的页面
- (UIViewController *)acquireViewControllerWithParas:(NSDictionary *)paras {
    return nil;
}

/// 设置页面
- (UIViewController *)acquireMineSettingPageWithParas:(NSDictionary *)paras {
    MineSecondViewController *vc = [MineSecondViewController new];
    vc.name = paras[@"name"];
    vc.bgColor = paras[@"bgColor"];
    return vc;
}

/// 二维码页面
- (UIViewController *)acquireQRCodePageWithParas:(NSDictionary *)paras {
    UIViewController *vc = [NSClassFromString(@"MineThirdViewController") new];
    [vc setValue:paras[@"name"] forKey:@"name"];
    vc.view.backgroundColor = paras[@"bgColor"];
    return vc;
}


@end
